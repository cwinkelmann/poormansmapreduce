<?php
require_once '../../src/app/classes/InputReader.php';
require_once '../../src/app/classes/RandomReader.php';

require_once '../../src/app/classes/Sampler.php';
require_once '../../src/app/classes/FixedFileSampler.php';

require_once '../../src/app/classes/SampleDictionary.php';

require_once '../../src/app/classes/RandomGenerator.php';

require_once '../../src/app/classes/exceptions/WrongElementCountException.php';

/**
 *
 */
class RandomReaderTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider providerGetSample
     * @group testGetSample
     */
    public function testGetSample($input, $exp)
    {
        $reader = new RandomReader();
        $sampler = new Sampler($reader);
        $reader->readCharacters(10000);
        $sample = $sampler->getSample($input);

        $this->assertEquals($exp, strlen($sample));
    }

    public function providerGetSample()
    {
        return array(
            array(5,
                5)
        );
    }

    /**
     * @dataProvider providerGetAsample
     * @group testGetSample_mock
     */
    public function testGetSample_mock($input, $exp)
    {
        // Create a stub for the SomeClass class.
        $stub = $this->getMockBuilder('RandomReader')
            ->getMock();

        // Configure the stub.
        $stub->method('getRandomElement')
            ->willReturn('a');


        $reader = new RandomReader();
        $sampler = new Sampler($reader);

        $sampler->setReader($stub);

        $sample = $sampler->getSample($input);

        $this->assertEquals($exp, $sample);
    }

    public function providerGetAsample()
    {
        return array(
            array(
                5,
                'aaaaa'
            ),
            array(
                10,
                'aaaaaaaaaa'
            ),
        );
    }


}
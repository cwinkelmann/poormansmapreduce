<?php
require '../../src/app/classes/InputReader.php';
require '../../src/app/classes/RandomReader.php';

require_once '../../src/app/classes/Sampler.php';
require_once '../../src/app/classes/FixedFileSampler.php';

require_once '../../src/app/classes/SampleDictionary.php';

require_once '../../src/app/classes/RandomGenerator.php';

require_once '../../src/app/classes/exceptions/WrongElementCountException.php';

/**
 *
 */
class RandomGeneratorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @group testGetRandomNumbersWithoutRepetition
     * @expectedException WrongElementCountException
     */
    public function testGetRandomNumbersWithoutRepetitionThrowException()
    {
        $random_generator = new RandomGenerator();
        new SampleDictionary('../test_data/cache.txt');
        $random_generator->getRandomNumbersWithoutRepetition(100, 10);

        $this->assertEquals(1,2);
    }

    /**
     * @group testGetRandomNumbersWithoutRepetition
     */
    public function testGetRandomNumbersWithoutRepetition()
    {
        $random_generator = new RandomGenerator();
        new SampleDictionary('../test_data/cache.txt');
        $positions = $random_generator->getRandomNumbersWithoutRepetition(3, 10);

        $this->assertEquals(3, count($positions));
    }


}
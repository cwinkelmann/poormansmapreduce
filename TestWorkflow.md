create a file of 26 random letters with random_generator.php
$ random_generator.php > data/random_input_uniform.txt

fold the string into one letter per line
$ fold -w 1 data/random_input_uniform.txt > data/random_input_uniform_fold.txt

load the input data into a database


CREATE TABLE input (
  letter varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SELECT * FROM random.input;

LOAD DATA LOCAL INFILE '/home/christian/development/PoorMansMapReduce/src/app/data/random_input_uniform_fold.txt'
    INTO TABLE input;

SELECT letter, count(1) FROM random.input 
GROUP BY letter;

#TRUNCATE input ;

Now generate a table for the cache and the output

execute the sampler using the generated and folded input and write the output into another txt file
$cat data/random_input_uniform.txt | php sampler.php 100000 > data/output.txt

Now you have all the data to evaluate the intermediate result and the output
<?php
include_once 'BaseReader.php';
include_once 'interfaces/inputInterface.php';

/**
    read a random string
 */
class RandomReader extends BaseReader implements inputInterface
{
    protected $count;
    private $input_generator;

    public function __construct()
    {
        parent::__construct();
        $this->input_generator = null;
        $this->count = 0;
    }


    public function readCharacters($n = 100000)
    {
        $this->count++;
        $this->buffer_size = null;
        $this->buffer = null;

        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        $buffer = '';
        for ($i = 0; $i < $n; $i++) {
            $buffer .= $characters[$this->random_generator->getRandomNumber(0, $charactersLength - 1)];
        }

        $this->buffer = $buffer;

        return mt_rand(0,100)%71 !== 0;
    }
}
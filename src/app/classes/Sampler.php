<?php

require_once 'RandomGenerator.php';

/**
 *    Sample Data from unbound input stream
 */
class Sampler
{
    /** @var BaseReader */
    private $reader;

    public function __construct(BaseReader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * read sample of fixed size from input reader
     *
     * @param int $k
     * @return string
     */
    public function getSample($k)
    {
        $buffer = array();
        for ($i = 0; $i < $k; $i++)
        {
            $buffer[] = $this->reader->getRandomElement();
        }

        return implode('', $buffer);
    }

    /**
     * @param BaseReader $reader
     */
    public function setReader($reader)
    {
        $this->reader = $reader;
    }
}
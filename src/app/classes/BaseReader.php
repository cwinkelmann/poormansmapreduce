<?php
require_once 'RandomGenerator.php';

/**
 * Base class collecting shared functions
 */
abstract class BaseReader
{
    protected $buffer_size;

    /**
     * @var array input buffer
     */
    protected $buffer;

    /**
     * @var RandomGenerator helper to take random values
     */
    protected $random_generator;

    public function __construct()
    {
        $this->random_generator = new RandomGenerator();
    }


    /**
     * calculate the temporary buffer size and return it
     *
     * @return int
     */
    public function getBufferSize()
    {
        if(!$this->buffer_size)
        {
            $this->buffer_size = count($this->buffer);
        }
        return $this->buffer_size;
    }

    /**
     * @return array
     */
    public function getBuffer()
    {
        return $this->buffer;
    }

    /**
     *
     * pick a random buffered element
     *
     * @return mixed
     *
     */
    public function getRandomElement()
    {
        $return_val = false;
        // pickung up random elements from an array is much easier than from a string
        if (is_string($this->buffer))
        {
            $this->buffer = str_split($this->buffer);
        }

        if (is_array($this->buffer))
        {
            $return_val = $this->buffer[$this->random_generator->getRandomNumber(0, $this->getBufferSize() - 1)];
        }

        return $return_val;
    }


}
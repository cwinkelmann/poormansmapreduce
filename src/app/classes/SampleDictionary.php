<?php

/**
 * Cache Class to Write all intermediate Samples to disk and keep the total Size
 */
class SampleDictionary
{
    private $sample_size = 0;

    public function __construct($file_name)
    {
        $this->handle = fopen($file_name, "w+");
    }

    /**
     * @param $sample
     */
    public function addSample($sample)
    {
        if (!is_string($sample) || strlen($sample) == 0)
        {
        }
        else
        {
            $this->sample_size += strlen($sample);

            fputs($this->handle, $sample);
        }

    }

    public function getSampleSize()
    {
        return $this->sample_size;
    }


}
<?php
require_once 'BaseReader.php';
require_once 'interfaces/inputInterface.php';

/**
 * read input from STDIN
 */
class InputReader extends BaseReader implements inputInterface
{
    private $input_generator;
    private $stdin;


    public function __construct()
    {
        parent::__construct();
        $this->input_generator = null;
        $this->stdin = fopen('php://stdin', 'r');
    }

    /**
     * read n elements
     *
     * @param int $n
     * @return string
     */
    public function readCharacters($n = 10000)
    {
        $this->buffer = stream_get_line($this->stdin, $n);
        if(is_string($this->buffer))
        {
            return true;
        }
        else
        {
            return false;

        }


    }
}
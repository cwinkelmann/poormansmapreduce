<?php

/**
 * Sample File Reader for Files of known length
 */
class FixedFileSampler
{
    /**
     * @var mixed file handle
     */
    private $file_handle;

    public function __construct($file, $filesize)
    {
        if (file_exists($file)) {
            $file_handle = fopen($file, 'r+');
        }
        else
        {
            throw new Exception("file does not exist");
        }
        $this->file_handle = $file_handle;
        $this->file_size = $filesize;
    }

    /**
     * from a given
     *
     * @param array $positions
     * @return SplFixedArray
     */
    public function getSample(array $positions)
    {
        sort($positions);

        $buffer = new SplFixedArray(count($positions));
        foreach ($positions as $key => $value)
        {
            fseek($this->file_handle, $value);

            // read some data
            $buffer[$key] = fgets($this->file_handle, 2);
        }

        return $buffer;
    }
}
<?php

interface inputInterface
{
    /**
     * read a fixed amount of character from the input
     *
     * @param int $n
     * @return mixed
     */
    public function readCharacters($n = 10000);


    /**
     * @return mixed
     */
    public function getRandomElement();
}
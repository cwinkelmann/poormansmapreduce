<?php

require_once 'exceptions/WrongElementCountException.php';

/**
 * Function to help with generation random Data
 *
 * Class RandomGenerator
 *
 */
class RandomGenerator
{
    protected $number_list = array();

    /**
     * generate a list of random numbers
     *
     * @param $k
     * @param $range
     * @return array
     * @throws Exception
     */
    public function getRandomNumbersWithoutRepetition($k, $range)
    {
        if($k > $range)
        {
            throw new WrongElementCountException("you cannot take more elements than you have");
        }
        $number_list = array();

        while(count($number_list) < $k)
        {
            $number_list[mt_rand(0, $range - 1)] = 0;
        }

        return array_keys($number_list);
    }

    /**
     * get a random number
     *
     * @param $min
     * @param $max
     * @return int
     */
    public function getRandomNumber($min, $max)
    {
        return mt_rand($min, $max);
    }
}
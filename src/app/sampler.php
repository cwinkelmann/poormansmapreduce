<?php
/**
 * script to pick random values from a huge input stream
 *
 * its simply reading the input into chunks of a fixed size and picking a random sample from these chunks
 * these samples are flushed to disk and only a counter, which counts the element stays in memory
 * with that knowledge random elements can be picked after the input stream ends
 */

require_once 'classes/InputReader.php';
require_once 'classes/RandomReader.php';

require_once 'classes/Sampler.php';
require_once 'classes/FixedFileSampler.php';

require_once 'classes/SampleDictionary.php';

require_once 'classes/RandomGenerator.php';

const READER_BUFFER_SIZE = 500000;   // influences how many element will be read into a local buffer
const CACHE_FILE = './data/cache.txt'; // cache file location


$k = $argv[1];
$random_input = isset($argv[2]) ? $argv[2] : false;

if (is_null($random_input))
{
    $reader = new InputReader();
}
else
{
    $reader = new RandomReader();
}

$sampler = new Sampler($reader);
$sample_dictionary = new SampleDictionary(CACHE_FILE);
$random_generator = new RandomGenerator();

while($reader->readCharacters(READER_BUFFER_SIZE) )
{
    $sample = $sampler->getSample($k);
    $sample_dictionary->addSample($sample);
}


$fixed_file_sampler = new FixedFileSampler(CACHE_FILE, $sample_dictionary->getSampleSize());

$positions = $random_generator->getRandomNumbersWithoutRepetition($k, $sample_dictionary->getSampleSize());


print implode('', $fixed_file_sampler->getSample($positions)->toArray()) . "\n";


print "max memory usage: " . round(memory_get_peak_usage() / 1024 / 1024) . "MB\n";



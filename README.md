# Sample random Character from a very huge input

### Assumptions 
* all elements from the sample should be stochastically independent
* the taken sample should have uniform distribution
* the size of the input is unknown
* the program should use constant memory but should be configurable

### Configuration in sampler.php
READER_BUFFER_SIZE = 500000 // influences how many element will be read into a local buffer
CACHE_FILE = './data/cache.txt'; // cache file location


### Usage 
There are two possible ways of using this. Change into src/app
#### Just get some random values from you bash and return 5 random variable
cat /dev/urandom | base64 | head -c 500000 | php sampler.php 5

or

/dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 100 | php sampler.php 5

#### Let the php sampler generate its own random values
php sampler.php 5 1
to influence the amount of data generated this

RandomReader.php -> return mt_rand(0,100)%71 !== 0;


#### other
to test and reproduce random data the random_generator.php is quite helpful


### Limitations
* the samples are not completely uniform. If two subsample are taken but both are of different size, i.e. 10 and 3 because of an input of 13 elements length, the latter elements have a higher chance of ending up in the sample.
* a working autoloader would have been a good idea but with that application size its absence is ok